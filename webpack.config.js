'use strict';
const path = require('path'),
      nodeExternals = require('webpack-node-externals');

module.exports = [
  {
    entry: './dev/app.js',
    mode: 'production',
    target: 'node',
    externals: [nodeExternals()],
    output: {
      path: path.resolve(__dirname, './'),
      filename: 'app.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          query: {
            presets: ['env']
          },
          test: /\.js$/,
          exclude: /node_modules/
        }
      ]
    }
  },
  {
    entry: './dev/modules/js/validator.js',
    mode: 'production',
    target: 'web',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'validator.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          query: {
            presets: ['env']
          },
          test: /\.js$/,
          exclude: /node_modules/
        }
      ]
    }
  },
  {
    entry: './dev/modules/js/paginator.js',
    mode: 'production',
    target: 'web',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'paginator.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          query: {
            presets: ['env']
          },
          test: /\.js$/,
          exclude: /node_modules/
        }
      ]
    }
  },
  {
    entry: './dev/modules/js/sort.js',
    mode: 'production',
    target: 'web',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'sort.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          query: {
            presets: ['env']
          },
          test: /\.js$/,
          exclude: /node_modules/
        }
      ]
    }
  },
  {
    entry: './dev/modules/js/accordeon.js',
    mode: 'production',
    target: 'web',
    output: {
      path: path.resolve(__dirname, './public'),
      filename: 'accordeon.js'
    },
    module: {
      rules: [
        {
          loader: 'babel-loader',
          query: {
            presets: ['env']
          },
          test: /\.js$/,
          exclude: /node_modules/
        }
      ]
    }
  }

];
