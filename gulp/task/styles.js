'use strict';
const gulp = require('gulp'),
      postcss = require('gulp-postcss'),
      cssImport = require('postcss-import'),
      cssVars = require('postcss-simple-vars'),
      nestedCss = require('postcss-nested'),
      autoprefixer = require('autoprefixer'),
      mixins = require('postcss-mixins');

gulp.task('styles', () => {
  return gulp.src('./dev/main.css')
    .pipe(postcss([cssImport, mixins, cssVars, nestedCss, autoprefixer]))
    .on('error', function(error) {
      console.log(error.message);
      this.emit('end');
    })
    .pipe(gulp.dest('./public'));
});
