'use strict';
const gulp = require('gulp'),
      watch = require('gulp-watch'),
      browserSync = require('browser-sync'),
      gls = require('gulp-live-server'),
      server = gls.new('./app.js');

gulp.task('watch', () => {

  browserSync.init(null, {
    proxy: 'http://localhost:3000',
        files: ['public/**/*.*', 'views/**/*.*'],
        port: 7000,
  });

  watch('app.js', () => {
    server.stop();
    server.start();
    browserSync.reload();
  });

  watch('./views/**/.ejs', () => {
    browserSync.reload();
  });

  watch('./dev/**/*.css', () => {
    gulp.start('cssInject');
  });

  watch('./dev/**/*.js', () => {
    gulp.start('scripts');
  });
});

gulp.task('cssInject', ['styles'], () => {
  return gulp.src('./public/main.css')
    .pipe(browserSync.stream());
});

gulp.task('default', ['watch'], function () {
  return server.start();
});
