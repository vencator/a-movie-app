import { years } from '../modules/years';
const express = require('express'),
      router = express.Router(),
      request = require('request');

router.get('/', (req, res) => {
  res.render('landing', { title: 'Movie', years: years, paginData: null, error: null });
});

router.post('/search', (req, res) => {
  res.redirect('/search/'+ req.body.searchString + '/page/1');
});

router.get('/search/:searchString/page/:page', (req, res) => {
  const url = 'http://www.omdbapi.com/?apikey=cc4f755e&s=' + encodeURI(req.params.searchString) + '&page=' + req.params.page;
  console.log(url);
  request.get(url , function (error, response, body) {
    const data = JSON.parse(body);
    if (error) {
      return console.log('Something is wrong', error);
    }
    if (data.Error) {
      console.log(data.Error);
      res.render('landing', {
        error: data.Error,
        title: data.Error,
        years: years,
        paginData: null
      });
    } else {
      const paginData = {
        searchString: req.params.searchString,
        currentPage: req.params.page,
        total: JSON.parse(body).totalResults
      };
      res.render('landing',
      {
        title: 'Movie',
        years: years,
        error: null,
        results: JSON.parse(body).Search,
        paginData: paginData
      });
    }
  });
});

export { router };
