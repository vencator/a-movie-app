'use strict';
import { router as indexRoutes } from './routes/index';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/", indexRoutes);
app.get('*', (req, res) => {
  res.send('Page not found 404!');
});
app.listen(3000);
