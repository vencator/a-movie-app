const thisYear = (new Date()).getFullYear();
let years = [];
for (let i = thisYear; i >= 1878; i-- ) {
  years.push(i);
}

export { years };
