'use strict';

const acc = document.getElementsByClassName('accordeon');

for (let i = 0; i < acc.length; i++) {
  acc[i].addEventListener('click', function() {
    console.log(this.classList.contains('active'));
    let isThereActive = false;
    for (let i = 0; i < acc.length; i++) {  // check some button has active class
      if (acc[i].classList.contains('active')) {
        isThereActive = true;
      }
    }
    const panel = this.nextElementSibling;
    if (!isThereActive) {  // if no button has active class add class to this button and open panel
      this.classList.add('active');
      panel.style.maxHeight = panel.scrollHeight + 'px';
    } else {  // if some button has active class
      if (this.classList.contains('active')) {  // if this button has active class, remove class and close panel
        this.classList.remove('active');
        panel.style.maxHeight = null;
      } else {  // if this has not active remove active from all other buttons and close panels the add active to this button
        for (let i = 0; i < acc.length; i++) {
          acc[i].classList.remove('active');
          acc[i].nextElementSibling.style.maxHeight = null;
        }
        this.classList.add('active');
        panel.style.maxHeight = panel.scrollHeight + 'px';
      }
    }
    const url = 'http://www.omdbapi.com/?apikey=cc4f755e&i=' + panel.dataset.id;
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       const data = JSON.parse(xhttp.responseText);
       const details = document.querySelector('[data-id=' + data.imdbID + ']');
       const fields = details.querySelectorAll('.info > span');
       for (let field of fields) {
         field.innerHTML = '<strong>' + field.getAttribute('id') + ':</strong> ' + data[field.getAttribute('id')];
       }
       const isIE = /*@cc_on!@*/false || !!document.documentMode;
       const isEdge = !isIE && !!window.StyleMedia;
       const activeButton = document.querySelector('.accordeon.active');
       if (!isEdge && activeButton) {
        setTimeout(function() {
          console.log(this);
          const bodyRect = document.body.getBoundingClientRect();
          const rect = activeButton.getBoundingClientRect();
          const offset = rect.top - bodyRect.top;
          console.log(offset, rect.top, bodyRect.top);
          window.scrollTo({
            top: offset,
            behavior: 'smooth'
          });
         }, 200);
       }
     }
   };
   xhttp.onerror = function () {
      console.log('** An error occurred during the transaction');
    };
   xhttp.open('GET', url);
   xhttp.send();
  });
}
