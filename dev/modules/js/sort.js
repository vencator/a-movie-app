'use strict';
console.log('sort!!');
const sortContainer = document.querySelector('.sort');
const buttonNames = ['def', 'AZ', 'ZA', 'yr↓', 'yr↑'];
let sortableItems = document.querySelectorAll('.accordeon-list > li');
const defaultSort = sortableItems;

// create sort buttons
for (let i = 0; i < buttonNames.length; i++) {
  const button = document.createElement('button');
  i === 0 ? button.classList.add('active') : false;
  button.textContent = buttonNames[i];
  sortContainer.appendChild(button);
  button.addEventListener('click', function() {
    sort(i);
    const buttons = sortContainer.querySelectorAll('button');
    for (let index = 0; index < buttons.length; index++) {
      buttons[index].classList.remove('active'); 
    }
    buttons[i].classList.add('active');
  });
}

function sort(style) {
  switch(style) {
    case 0:
      sortDefault();
      break;
    case 1:
      sortAZ();
      break;
    case 2:
      sortZA();
      break;
    case 3:
      sort19();
      break;
    case 4:
      sort91();
      break;
  }
}

function sortDefault() {
  let sortableItemsArray = [].slice.call(defaultSort);
  sortableItemsArray.forEach(function(item) {
    document.querySelector('.accordeon-list').appendChild(item);
  });
}

function sortAZ() {
  let sortableItemsArray = [].slice.call(sortableItems).sort(function (a, b) {
    return a.textContent > b.textContent ? 1 : -1;
  });
  sortableItemsArray.forEach(function(item) {
    document.querySelector('.accordeon-list').appendChild(item);
  });
}

function sortZA() {
  let sortableItemsArray = [].slice.call(sortableItems).sort(function (a, b) {
    return a.textContent < b.textContent ? 1 : -1;
  });
  sortableItemsArray.forEach(function(item) {
    document.querySelector('.accordeon-list').appendChild(item);
  });
}

function sort19() {
  let sortableItemsArray = [].slice.call(sortableItems).sort(function (a, b) {
    return a.querySelector('span').textContent > b.querySelector('span').textContent ? 1 : -1;
  });
  sortableItemsArray.forEach(function(item) {
    document.querySelector('.accordeon-list').appendChild(item);
  });
}

function sort91() {
  let sortableItemsArray = [].slice.call(sortableItems).sort(function (a, b) {
    return a.querySelector('span').textContent < b.querySelector('span').textContent ? 1 : -1;
  });
  sortableItemsArray.forEach(function(item) {
    document.querySelector('.accordeon-list').appendChild(item);
  });
}
