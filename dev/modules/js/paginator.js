'use strict';
const paginator = document.querySelector('.paginator');
const paginData = JSON.parse(paginator.dataset.pagindata);

// check if element .paginator #pag-'some number' exists

if (paginator) {
  createControls(parseInt(paginData.total));

} else {
  console.log('Element with class paginator doesn\'t have a correct data-pagindata');
}

function createControls(totalResults) {
  const searchString = paginData.searchString;
  const totalPages = Math.ceil(totalResults / 10);
  let   currentPage = parseInt(paginData.currentPage);
  const prevControlButtonsContainer = document.createElement('span'),
        pageNumberButtonsContainer = document.createElement('span'),
        nextControlButtonsContainer = document.createElement('span');
  prevControlButtonsContainer.classList.add('prev-container');
  pageNumberButtonsContainer.classList.add('number-container');
  nextControlButtonsContainer.classList.add('next-container');
  paginator.appendChild(prevControlButtonsContainer);
  paginator.appendChild(pageNumberButtonsContainer);
  paginator.appendChild(nextControlButtonsContainer);
  const buttons = {
    firstButton: document.createElement('a'),
    bigPrevButton: document.createElement('a'),
    prevButton: document.createElement('a'),
    nextButton: document.createElement('a'),
    bigNextButton: document.createElement('a'),
    lastButton: document.createElement('a')
  };
  buttons.firstButton.textContent = '<<<';
  buttons.firstButton.setAttribute('href', '/search/' + searchString + '/page/' + 1);
  buttons.bigPrevButton.textContent = '<<';
  buttons.bigPrevButton.setAttribute('href', '/search/' + searchString + '/page/' + (currentPage - 10));
  buttons.prevButton.textContent = '<';
  buttons.prevButton.setAttribute('href', '/search/' + searchString + '/page/' + (currentPage - 1));
  buttons.nextButton.textContent = '>';
  buttons.nextButton.setAttribute('href', '/search/' + searchString + '/page/' + (currentPage + 1));
  buttons.bigNextButton.textContent = '>>';
  buttons.bigNextButton.setAttribute('href', '/search/' + searchString + '/page/' + (currentPage + 10));
  buttons.lastButton.textContent = '>>>';
  buttons.lastButton.setAttribute('href', '/search/' + searchString + '/page/' + totalPages);
  prevControlButtonsContainer.appendChild(buttons.firstButton);
  prevControlButtonsContainer.appendChild(buttons.bigPrevButton);
  prevControlButtonsContainer.appendChild(buttons.prevButton);
  nextControlButtonsContainer.appendChild(buttons.nextButton);
  nextControlButtonsContainer.appendChild(buttons.bigNextButton);
  nextControlButtonsContainer.appendChild(buttons.lastButton);
  createPageButtons(totalPages, pageNumberButtonsContainer, searchString, currentPage);
  addActiveClass(currentPage, pageNumberButtonsContainer);
  disableUnusableButtons(currentPage, buttons, totalPages);
}

function createPageButtons(totalPages, container, searchString, currentPage) {
  let iterations;
  let remainingPages = totalPages - currentPage;
  const startIteration = parseInt(((Math.trunc((currentPage - 1)/ 10)).toString() + 1));
  const endIteration = totalPages - startIteration + 1;
  totalPages > 10 ? iterations = 10 : iterations = totalPages;
  if (iterations > remainingPages && iterations > endIteration) {
    iterations = endIteration;
  }
  for (let i = startIteration; i < startIteration + iterations; i++) {
    const button = document.createElement('a');
    button.textContent = i;
    button.setAttribute('href', '/search/' + searchString + '/page/' + i);
    container.appendChild(button);
    button.addEventListener('click', () => {
      addActiveClass(i, container);
    });
  }
}

function addActiveClass(currentPage, container) {
  const buttons = container.querySelectorAll('a');
  for (let i = 0; i < buttons.length; i++) {
    buttons[i].classList.remove('active');
  }
  let index = (currentPage.toString().slice(-1)) - 1;
  index < 0 ? index = buttons.length - 1 : false;
  buttons[index].classList.add('active');
}

function disableUnusableButtons(currentPage, buttons, totalPages) {
  if (currentPage === 1) {
    buttons.firstButton.classList.add('disabled');
    buttons.bigPrevButton.classList.add('disabled');
    buttons.prevButton.classList.add('disabled');
  }
  if (currentPage <= 10) {
    buttons.bigPrevButton.classList.add('disabled');
  }
  if (currentPage === totalPages ) {
    buttons.lastButton.classList.add('disabled');
    buttons.bigNextButton.classList.add('disabled');
    buttons.nextButton.classList.add('disabled');
  }
  if (totalPages - currentPage < 10) {
    buttons.bigNextButton.classList.add('disabled');
  }
}
