const searchInput = document.querySelector('.search-box input');
const submitButton = document.querySelector('.search-box button');

submitButton.setAttribute('disabled', 'true');
searchInput.addEventListener('input', function() {
  searchInput.value.length  > 1 ? submitButton.removeAttribute('disabled') : submitButton.setAttribute('disabled', 'true');
});