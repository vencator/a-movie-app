# OMDB App

An app for searching info about movies and games. Created with JS, Express, Gulp, BrowserSync, Postcss, Webpack, Babel.

## Getting Started

install - `npm i`
run project - `gulp`
generate css - `gulp styles`
generate js - `gulp scripts`

### Prerequisites

Node.js
NPM
Gulp

## Author

* **Václav Ryšavý**


## License

This project is licensed under the ICS License.

## Acknowledgments

* Learning
* Inspiration
* etc
